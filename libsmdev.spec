Name: libsmdev
Version: 20190315
Release: 1
Summary: Library to access and read storage media (SM) devices
Group: System Environment/Libraries
License: LGPL
Source: %{name}-%{version}.tar.gz
URL: https://github.com/libyal/libsmdev
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
       
BuildRequires: gcc       

%description -n libsmdev
Library to access and read storage media (SM) devices

%package -n libsmdev-static
Summary: Library to access and read storage media (SM) devices
Group: Development/Libraries
Requires: libsmdev = %{version}-%{release}

%description -n libsmdev-static
Static library version of libsmdev.

%package -n libsmdev-devel
Summary: Header files and libraries for developing applications for libsmdev
Group: Development/Libraries
Requires: libsmdev = %{version}-%{release}

%description -n libsmdev-devel
Header files and libraries for developing applications for libsmdev.

%package -n libsmdev-python2
Obsoletes: libsmdev-python < %{version}
Provides: libsmdev-python = %{version}
Summary: Python 2 bindings for libsmdev
Group: System Environment/Libraries
Requires: libsmdev = %{version}-%{release} python2
BuildRequires: python2-devel

%description -n libsmdev-python2
Python 2 bindings for libsmdev

%package -n libsmdev-python3
Summary: Python 3 bindings for libsmdev
Group: System Environment/Libraries
Requires: libsmdev = %{version}-%{release} python3
BuildRequires: python3-devel

%description -n libsmdev-python3
Python 3 bindings for libsmdev

%package -n libsmdev-tools
Summary: Several tools for accessing storage media (SM) devices
Group: Applications/System
Requires: libsmdev = %{version}-%{release}

%description -n libsmdev-tools
Several tools for accessing storage media (SM) devices

%prep
%setup -q

%build
%configure --prefix=/usr --libdir=%{_libdir} --mandir=%{_mandir} --enable-python2 --enable-python3
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%make_install

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -n libsmdev
%defattr(644,root,root,755)
%license COPYING
%doc AUTHORS README
%attr(755,root,root) %{_libdir}/*.so.*

%files -n libsmdev-static
%defattr(644,root,root,755)
%license COPYING
%doc AUTHORS README
%attr(755,root,root) %{_libdir}/*.a

%files -n libsmdev-devel
%defattr(644,root,root,755)
%license COPYING
%doc AUTHORS README
%{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/pkgconfig/libsmdev.pc
%{_includedir}/*
%{_mandir}/man3/*

%files -n libsmdev-python2
%defattr(644,root,root,755)
%license COPYING
%doc AUTHORS README
%{_libdir}/python2*/site-packages/*.a
%{_libdir}/python2*/site-packages/*.la
%{_libdir}/python2*/site-packages/*.so

%files -n libsmdev-python3
%defattr(644,root,root,755)
%license COPYING
%doc AUTHORS README
%{_libdir}/python3*/site-packages/*.a
%{_libdir}/python3*/site-packages/*.la
%{_libdir}/python3*/site-packages/*.so

%files -n libsmdev-tools
%defattr(644,root,root,755)
%license COPYING
%doc AUTHORS README
%attr(755,root,root) %{_bindir}/*
%{_mandir}/man1/*

%changelog
* Sat Mar 16 2019 Joachim Metz <joachim.metz@gmail.com> 20190315-1
- Auto-generated

